const express = require('express');
const router = express.Router();
const {
  createTodo,
  updateTodo,
  getTodo,
  deleteTodo,
  clearCompleted
} = require('../controllers/TodoController');


//createTodo
router.post('/createTodo', createTodo);

//updateTodo
router.post('/updateTodo', updateTodo);

//getTodo
router.post('/todos', getTodo);

//deleteTodo
router.post('/deleteTodo', deleteTodo);

//clearCompleted
router.post('/clearCompleted', clearCompleted);

module.exports = router;