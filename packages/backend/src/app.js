const express = require('express');
const allRoutes = require('express-list-endpoints');

const router = require('./router');

const app = express();
const port = 3000;

app.use('api', router);

app.listen(port, () => {
  console.log(`Todo app listening on port ${port}!`);
  console.log('Registered Routes: ');
  console.log(allRoutes(app));
});

