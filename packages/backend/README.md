# Use stories

```
1 - Người dùng có thể tạo nhiều todo item
2 - Người dùng có thể mark done 1 hoặc nhiều todo item
3 - Người dùng có thể xem toàn bộ todo (Bao gồm đã hoàn thành và chưa hoàn thành)
4 - Người dùng có thể filter những todo đã hoàn thành
5 - Người dùng có thể filter những todo chưa hoàn thành
6 - Người dùng có thể xóa các todo
7 - Người dùng có thể xóa toàn bộ những todo đã hoàn thành
8 - Người dùng có thể sửa nội dung todo.
```

# Use build APIs
```
1 - /api/createTodo
2 - /api/updateTodo
3 - /api/todos
4 - /api/todos with params: completed: boolean
5 - /api/todos with params: completed: false
6 - /api/deleteTodo
7 - /api/clearCompleted
```